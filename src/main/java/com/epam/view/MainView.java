package com.epam.view;

import com.epam.controller.TaskController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class MainView {

    private static Logger log = LogManager.getLogger(MainView.class);

    private Locale locale;
    private ResourceBundle bundle;
    private TaskController taskController;
    private Map<String, String> menu;
    private Map<String, Runnable> menuOption;
    private Map<String, String> menuMethods;

    private void setMenu() {
        taskController = new TaskController();
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("exit"));
    }

    private void setMenuMethods() {
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", bundle.getString("m1"));
        menuMethods.put("2", bundle.getString("m2"));
        menuMethods.put("3", bundle.getString("m3"));
        menuMethods.put("4", bundle.getString("m4"));
        menuMethods.put("5", bundle.getString("m5"));
        menuMethods.put("6", bundle.getString("m6"));
        menuMethods.put("7", bundle.getString("m7"));
        menuMethods.put("8", bundle.getString("m8"));
        menuMethods.put("9", bundle.getString("m9"));
        menuMethods.put("10", bundle.getString("m10"));
        menuMethods.put("11", bundle.getString("m11"));
        menuMethods.put("12", bundle.getString("m12"));
        menuMethods.put("13", bundle.getString("m13"));
        menuMethods.put("14", bundle.getString("m14"));
        menuMethods.put("17", bundle.getString("return"));
    }

    public MainView() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MainView", locale);
        setMenu();
        setMenuMethods();
        menuOption = new LinkedHashMap<>();
        menuOption.put("1", this::showMethodMenu);
        menuOption.put("2", () -> switchInternationalMenu("en"));
        menuOption.put("3", () -> switchInternationalMenu("uk"));
        menuOption.put("4", () -> switchInternationalMenu("de"));
    }

    public void show() {
        Scanner scanner = new Scanner(System.in);
        String flagMessage;
        do {
            menu.values().forEach(s -> log.info(s));
            flagMessage = scanner.next();
            try {
                menuOption.get(flagMessage).run();
            } catch (NullPointerException e) {
                log.info(bundle.getString("exit") + "\n");
            }
        } while (!flagMessage.equals("q"));
    }

    private void showMethodMenu() {
        Scanner scanner = new Scanner(System.in);
        String flagMessage;
        do {
            menuMethods.values().forEach(s -> log.info(s));
            flagMessage = scanner.next();
            try {
                taskController.getMethod(flagMessage);
            } catch (NullPointerException e) {
                log.info(bundle.getString("returned") + "\n");
            }
        } while (!flagMessage.equals("r"));
    }

    private void switchInternationalMenu(String language) {
        locale = new Locale(language);
        bundle = ResourceBundle.getBundle("MainView", locale);
        setMenu();
        setMenuMethods();
    }
}
