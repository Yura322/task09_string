package com.epam.service.impl;

import com.epam.model.Text;
import com.epam.model.Word;
import com.epam.service.TaskService;
import com.epam.service.TextGenerateService;
import com.epam.model.Sentence;
import com.epam.service.util.CheckUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TaskServiceImpl implements TaskService {

    private static Logger log = LogManager.getLogger(TaskServiceImpl.class);
    private Text text;
    private List<Sentence> sentences;

    public TaskServiceImpl() {
        TextGenerateService generateService = new TextGenerateServiceImpl();
        this.text = generateService.generateText();
        this.sentences = text.getSentencesList();
    }

    @Override
    public void countSentenceRepeatWord() {
        int count;
        count = (int) sentences.stream()
                .filter(s -> s.getWordList().size() != s.getWordList()
                        .stream()
                        .distinct()
                        .count())
                .count();
        log.info(count);
    }

    @Override
    public void showSentenceByWordNumber() {
        Comparator<Sentence> comparator = Comparator.comparing(s1 -> s1.getWordList().size());
        sentences.stream().sorted(comparator).forEach(sentence -> log.info(sentence.getSentence()));
    }

    @Override
    public void interrogativeSentence(int wordLength) {
        List<Sentence> interrogativelist = sentences.stream()
                .filter(s -> s.getSentence()
                        .matches("[^.?!]+[?]"))
                .collect(Collectors.toList());
        interrogativelist.forEach(sentence -> sentence.getWordList()
                .stream()
                .distinct()
                .filter(word -> word.getWord().length() == wordLength)
                .forEach(word -> log.info(word.getWord())));
    }

    @Override
    public void replaceWordForLongestWord() {
        String getFirstVowelWord = "(\\b[aiouey][^\\s,.!?]+)";
        List<String> newSentence = new ArrayList<>();
        for (Sentence s : sentences) {
            Word longestWord = s.getWordList().stream()
                    .max(Comparator.comparingInt(w -> w.getWord().length()))
                    .orElse(new Word("null"));
            log.debug(longestWord.getWord());
            newSentence.add(s.getSentence()
                    .replaceFirst(getFirstVowelWord, longestWord.getWord()));
        }
        newSentence.forEach(log::info);
    }

    @Override
    public void deleteWordByLength(int length) {
        int lengthWord = length - 1;
        String newText = text.getText()
                .replaceAll("\\b[^\\saioueyAUIEOY][^\\W]{" + lengthWord + "}\\b", "").trim();
        log.info(newText);
    }

    @Override
    public void findUniqueWordInFirstSentence() {
        Word uniqueWord = sentences.stream()
                .limit(1)
                .flatMap(sentence -> sentence.getWordList().stream()
                        .filter(word -> sentences.stream()
                                .skip(1).noneMatch(sentence1 -> sentence1.getSentence()
                                        .contains(word.getWord()))))
                .findFirst().orElse(new Word(""));
        log.info(uniqueWord.getWord());
    }

    @Override
    public void sortWordsByAlphabet() {
        List<String> wordsList = text.getWordsList().stream()
                .map(word -> word.getWord().toLowerCase())
                .sorted()
                .collect(Collectors.toList());
        wordsList.forEach(w -> log.info(w));
    }

    @Override
    public void sortVowelWordByConsonants() {
        List<Word> wordList = text.getWordsList().stream()
                .filter(word -> word.getWord().matches("(\\b[aiouey][^\\s,.!?]+)"))
                .sorted(Comparator.comparing(word -> CheckUtil.checkFirstConsonants(word.getWord())))
                .collect(Collectors.toList());
        wordList.forEach(word -> log.info(word.getWord()));
    }

    @Override
    public void sortByLetterCount(char letter) {
        List<String> wordList = text.getWordsList().stream()
                .map(word -> word.getWord().toLowerCase())
                .sorted()
                .distinct()
                .sorted(Comparator.comparing(word -> CheckUtil.checkLetterCount(word, letter)))
                .collect(Collectors.toList());
        wordList.forEach(word -> log.info(word));
    }

    @Override
    public void swapWordInSentence(int sentenceNumber, int wordLength, String newWord) {
        log.info(text.getSentencesList().size());
        if (text.getSentencesList().size() >= sentenceNumber - 1) {
            String word = text.getSentencesList().get(sentenceNumber - 1)
                    .getSentence()
                    .replaceAll("\\b[\\w]{" + wordLength + "}\\b", newWord);
            log.info(word);
        } else {
            log.warn("Sentence not found");
        }
    }

    @Override
    public void sortByLetterCountReverse(char letter) {
        List<String> wordList = text.getWordsList().stream()
                .map(word -> word.getWord().toLowerCase())
                .sorted()
                .distinct()
                .sorted(Comparator.comparing(word -> CheckUtil.checkLetterCount((String) word, letter)).reversed())
                .collect(Collectors.toList());
        wordList.forEach(word -> log.info(word));
    }

    @Override
    public void deleteFirstLastLetterFromWord() {
        List<String> wordList = text.getWordsList().stream()
                .map(word -> word.getWord().toLowerCase()
                        .replaceAll(String.valueOf(word.getWord().charAt(0)), "")
                        .replaceAll(String.valueOf(word.getWord().charAt(word.getWord().length() - 1)), ""))
                .collect(Collectors.toList());
        wordList.forEach(word -> log.info(word));
    }

    @Override
    public void deleteSubstringBy(char startLetter, char endLetter) {
        String newText = text.getText();
        int startIndex = 0;
        int endIndex;
        Matcher matcher = Pattern.compile(String.valueOf(startLetter)).matcher(newText);
        if (matcher.find()) {
            startIndex = matcher.start();
        }
        endIndex = newText.lastIndexOf(endLetter);
        log.info(newText.substring(0, startIndex) + newText.substring(endIndex, newText.length()));
    }

    @Override
    public void sortWordsByVowelPercent() {
        Map<String, Double> vowelPercentMap = new LinkedHashMap<>();
        text.getWordsList()
                .forEach(w -> vowelPercentMap.
                        put(w.getWord(), (double) CheckUtil.checkVowelCount(w.getWord()) / w.getWord().length()));
        vowelPercentMap.entrySet().stream()
                .sorted(Comparator.comparingDouble(Map.Entry::getValue))
                .forEach(entry -> log.info(entry.getKey() + " = " + entry.getValue()));
    }
}
