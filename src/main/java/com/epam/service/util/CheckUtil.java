package com.epam.service.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CheckUtil {

    public static char checkFirstConsonants(String word) {
        Matcher matcher = Pattern.compile("[^aeiouAEIOU]").matcher(word);
        if (matcher.find()) {
            return word.charAt(matcher.start());
        }
        return 1;
    }

    public static int checkVowelCount(String word){
        int count = 0;
       Matcher matcher = Pattern.compile("[aieouyAoiuye]").matcher(word);
       while (matcher.find()){
           count++;
       }
       return count;
    }

    public static int checkLetterCount(String word, char letter) {
        int count = 0;
        Matcher matcher = Pattern.compile(String.valueOf(letter)).matcher(word);
        while (matcher.find()) {
            count++;
        }
        return count;
    }
}
