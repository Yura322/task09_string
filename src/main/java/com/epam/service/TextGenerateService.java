package com.epam.service;

import com.epam.model.Text;

public interface TextGenerateService {

    Text generateText();
}
